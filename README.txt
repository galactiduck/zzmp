ZZMP: Extended Precision Fixed-Point Library for use in C/C++ and Fortran
===============================================================================

ZZMP is a library for large fixed-point numbers, supporting 112.16, 80.16, and
48.16 bit numbers. This means that the library can use 16 byte, 12 byte, and
8 byte numbers, where 16 bits of the numbers are used for sub-integer data. All
numbers are signed.

The API is intended to be usable from C, C++, and Fortran 77. It can be used
with the following C/C++ compilers: gcc, clang, tcc, OpenWatcom, MSVC. Probably
more, but they haven't been tested. It has been used with the following Fortran
compilers: gfortran, Solaris Studio, and OpenWatcom.

Most of the library is implemented in assembly, though some parts are
implemented in C (mostly division and square root functions).

The library is licensed under a variant of the BSD 4-clause, which includes an
"advertising" clause that requires all software that includes this software to
also include the notice "Trans rights are human rights" in the license text.

Building the library
-------------------------------------------------------------------------------

Building steps and requirements depend on the platform. Required tools are:

Unix amd64 (Linux, BSD, OS X, Solaris, etc):
 * One of gcc, clang, or Intel icc
 * GAS-compatible assembler

TODO! The following platforms are either incomplete or bitrotted:

Unix x86 (Linux, BSD, OS X, Solaris, etc):
 * One of gcc, clang, or tcc

Unix armv6 (Linux, OS X, BSD, etc):
 * One of gcc, clang

Unix aarch64 (Linux, OS X, BSD, etc):
 * One of gcc, clang, or Intel icc
 * Unified-syntax assembler (tested from GNU binutils)

Windows x86:
 * One of OpenWatcom 1.9, MSVC 5.0 or greater, gcc (tested from Cygwin)

Windows amd64:
 * One of MSVC 2012 or greater, GCC (tested from Cygwin)
 * Either yasm or nasm

Other architectures are possible, but implementations of the algorithms will
need to be written. Assembly is used because most platforms provide "extended
arithmetic" operations (add with carry, subtract with borrow, rotate with carry
bit etc), and extended multiplication (such as multiplying two 32-bit numbers
and getting a 64-bit result). See the `none` directory for some platform-
independent versions of some of the functions.

Using Unix make (GNU make or BSD make), the following variables can be set:

`ZZCC` Sets the C compiler. Must be nominally compatible with cc/gcc.
`ZZAR` Sets the ar tool.
`ZZRANLIB` Sets the ranlib tool. If not applicable, just set to touch or echo.
`ZZASM` Sets the assembler. Must be nominally compatible with GNU as.
`ZZYASM` Sets the yasm or nasm path. Used on x86 (32-bit) only.

Building for Unix on amd64
-------------------------------------------------------------------------------

The build system defaults to trying to use gcc. You need GNU make or BSD make.
To build, run:
`make ARCH=amd64`

To override the compiler (for instance, to use clang):
`make ARCH=amd64 ZZCC=clang`

The build system should be able to determine suitable compiler flags based on
the value of ZZCC.

Building for Unix or Cygwin on x86
-------------------------------------------------------------------------------

The build system defaults to trying to use gcc. You need GNU make or BSD make.
To build, run:
`make ARCH=x86`

To override the compiler (for instance, to use clang) or the assembler:
`make ARCH=x86 ZZCC=clang ZZYASM=nasm`

Building for Cygwin on amd64
-------------------------------------------------------------------------------

Make sure you have gcc (or clang, see below), GNU make, and yasm or nasm. To
build, run:
`make ARCH=amd64 OS=win32`

To override the compiler (for instance, to use clang) or the assembler:
`make ARCH=amd64 OS=win32 ZZCC=clang ZZYASM=nasm`

Building for x86 using Watcom
-------------------------------------------------------------------------------

TODO! This is currently not merged as it needs some fixing!

To build, enter the Watcom command prompt, navigate to the zzmp directory, and
run:

`wmake -fmakefile.wat -e -h`

To override the assembler (for instance, to use nasm instead of yasm):
`wmake -fmakefile.wat -e -h ZZYASM=nasm`

Building for x86 using Microsoft Visual C++
-------------------------------------------------------------------------------

TODO! This is currently not merged as it needs some fixing!

To build, enter the Visual Studio x86 or amd64 to x86 cross tools command
prompt, navigate to the zzmp directory, and run:

`nmake /nologo /fmakefile.msc ARCH=x86`

To override the assembler (for instance, to use nasm instead of yasm):
`nmake /nologo /fmakefile.msc ARCH=x86 ZZYASM=nasm`

Building for amd64 using Microsoft Visual C++
-------------------------------------------------------------------------------

TODO! This is currently not merged as it needs some fixing!

To build, enter the Visual Studio amd64 or x86 to amd64 cross tools command
prompt, navigate to the zzmp directory, and run:

`nmake /nologo /fmakefile.msc ARCH=amd64`

To override the assembler (for instance, to use nasm instead of yasm):
`nmake /nologo /fmakefile.msc ARCH=amd64 ZZYASM=nasm`

Shouldn't the 'MP' part mean it supports arbitrary precision?
-------------------------------------------------------------------------------

Yeah I kinda screwed up the name.

