#include "zzmp.h"

#ifdef ZZDEBUGLOG
# include <stdio.h>
#endif

/*****************************************************************************/
/* Software implementation of division. Uses the Miller algorithm. */

void zzmpsqrt4_(const unsigned a_[4], unsigned out[4]){
    unsigned ax[8];
    int i, off0, off1;
    
    ax[0] = 0;
    ax[1] = 0;
    ax[2] = 0;
    ax[3] = 0;
    i = zzmpbsr4_(a_);
#ifdef ZZDEBUGLOG
    printf("%i\t", i);
#endif
    if(i < 16){
        ax[0] = 0x10000;
    }
    else if(i == 16){
        if(0 && a_[0] == 0x10000){
            out[0] = 0x10000;
            out[1] = out[2] = out[3] = 0;
            return;
        }
        ax[0] = 0x16B00;
    }
    else{
        i -= 15;
        i >>= 1;
        i += 16;
        ax[i >> 5] |= 1 << (i & 31);
    }
#ifdef ZZDEBUGLOG
    printf("ax={%08X%08X%08X%08X}, a_={%08X%08X%08X%08X}\n",
        ax[3], ax[2], ax[1], ax[0],
        a_[3], a_[2], a_[1], a_[0]);
#endif
    off0 = 4;
    off1 = 0;
    do{
#if (defined __GNUC__) && ((defined __amd64) || (defined __x86_64__))
        __asm__ volatile(
            " xchgl %0, %1\n" : "+r"(off0), "+rm"(off1));
#else
        off0 = off1;
        off1 ^= 4;
#endif
#ifdef ZZDEBUGLOG
        printf("AA off0={%08X%08X%08X%08X}, off1={%08X%08X%08X%08X}\n",
            (ax+off0)[3], (ax+off0)[2], (ax+off0)[1], (ax+off0)[0],
            (ax+off1)[3], (ax+off1)[2], (ax+off1)[1], (ax+off1)[0]);
#endif
        zzmpdiv4_(a_, ax + off0, ax + off1);
#ifdef ZZDEBUGLOG
        printf("BB   a_={%08X%08X%08X%08X}, off1={%08X%08X%08X%08X}\n",
            a_[3], a_[2], a_[1], a_[0],
            (ax+off1)[3], (ax+off1)[2], (ax+off1)[1], (ax+off1)[0]);
#endif
#if (defined __GNUC__) && ((defined __amd64) || (defined __x86_64__))
        __asm__ volatile(
            " addq %2, %0\n" \
            " adcq %3, %1\n" \
            " rcrq $1, %1\n" \
            " rcrq $1, %0\n"
          : "+&r"(((unsigned long long*)(ax + off1))[0]), 
            "+&r"(((unsigned long long*)(ax + off1))[1])
          : "rm"(((unsigned long long*)(ax + off0))[0]), 
            "rm"(((unsigned long long*)(ax + off0))[1])
          : "cc");
#elif (defined __GNUC__) && ((defined __i386) || (defined __i386__))
        __asm__(
            " addl %4, %0\n" \
            " adcl %5, %1\n" \
            " adcl %6, %2\n" \
            " adcl %6, %3\n" \
            " rcrl $1, %3\n" \
            " rcrl $1, %2\n" \
            " rcrl $1, %1\n" \
            " rcrl $1, %0\n" \
  : "+r"(ax[off1+0]), "+r"(ax[off1+1]), "+r"(ax[off1+2]), "+r"(ax[off1+3])
  :  "mr"(ax[off0+0]),  "mr"(ax[off0+1]),  "mr"(ax[off0+2]),  "mr"(ax[off0+3])
  :  "cc");
#else
        zzmpadd4_(ax + off0, ax + off1, ax + off1);
        zzmpshr4_(ax + off1);
#endif
#ifdef ZZDEBUGLOG
        printf("CC off0={%08X%08X%08X%08X}, off1={%08X%08X%08X%08X}\n",
            (ax+off0)[3], (ax+off0)[2], (ax+off0)[1], (ax+off0)[0],
            (ax+off1)[3], (ax+off1)[2], (ax+off1)[1], (ax+off1)[0]);
#endif
    }while(zzmplt4_(ax + off1, ax + off0));
    /*
    out[0] = ax[off0 + 0] << 8;
    out[1] = (ax[off0 + 1] << 8) + ((ax[off0 + 0] >> 24) & 0xFF);
    out[2] = (ax[off0 + 2] << 8) + ((ax[off0 + 1] >> 24) & 0xFF);
    out[3] = (ax[off0 + 3] << 8) + ((ax[off0 + 2] >> 24) & 0xFF);
    */
    out[0] = ax[off0 + 0];
    out[1] = ax[off0 + 1];
    out[2] = ax[off0 + 2];
    out[3] = ax[off0 + 3];
}

