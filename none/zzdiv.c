#include "zzmp.h"
#include <assert.h>

/*****************************************************************************/
/* Software implementation of division. Uses the Miller algorithm.
 * See PowerPC Compiler Writer’s Guide as a canonical example.
 * Also as used in Clang's compiler-rt and GCC's libgcc.
 *
 * The PowerPC version should actually be reproduced in an ASM file, but we
 * don't have most of the machinery for PPC set up yet.
 */

void zzmpdiv2_(const unsigned a_[2], const unsigned b_[2], unsigned out[4]){
    int bits, neg;
    unsigned a[4], b[4];
    /* On 64-bit architectures with known endiannesses, do the division using
     * regular code when possible. */
#if (defined __GNUC__) || (defined __TINYC__) || (defined _MSC_VER)
/* Just disable warnings about long-long, we are using it only on very certain
 * platforms and compilers.
 */
# if (defined __clang__)
#   pragma clang diagnostic push
#   pragma clang diagnostic ignored "-Wlong-long"
# elif (defined __GNUC__) && !(defined __TINYC__)
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wlong-long"
# endif

# if (defined __amd64) || (defined __x86_64__) || (defined _M_AMD64) ||(defined __aarch64__) || (defined _M_ARM64)
    long long o;
    if(*(long long*)a_ == 0 || *(long long*)b_ == 0)
        return;
    if(*(long long*)a_ <= 0x00FFFFFFFFFFFFFFLL){
        o = ((*(long long*)a_) << 16) / *(long long*)b_;
        *(long long*)out = o;
        out[2] = out[3] = (o < 0) ? -1LL : 0LL;
        return;
    }
# elif (defined __sparcv9) || (defined __sparc_v9__) || (defined __ppc64__) || (defined _ARCH_PPC64)
    /* Big-endian, need to swap d-words. */
    long long ax, bx, o;
    if(a_[1] <= 0x00FFFFFF){
        ax = ((unsigned long long)(a_[0]) << 16) |
            ((unsigned long long)(a_[1]) << 48);
        bx = (unsigned long long)(b_[0]) | ((unsigned long long)(b_[1]) << 32);
        if(ax == 0 || bx == 0)
            return;
        o = ax / bx;
        out[0] = o;
        out[1] = o >> 32;
        out[2] = out[3] = (o < 0) ? -1LL : 0LL;
        return;
    }
# endif
# if (defined __clang__)
#   pragma clang diagnostic pop
# elif (defined __GNUC__) && !(defined __TINYC__)
#   pragma GCC diagnostic pop
# endif
#endif
    a[0] = a_[0];
    a[1] = a_[1];
    a[2] = a[3] = 0;
    
    b[0] = b_[0];
    b[1] = b_[1];
    b[2] = b[3] = 0;
    
    out[0] = out[1] = out[2] = out[3] = 0;
    if((a[1] | a[0]) == 0 || (b[1] | b[0]) == 0)
        return;
    neg = 0;
    if(zzmpabs2_(a))
        neg = !neg;
    if(zzmpabs2_(b))
        neg = !neg;
    if((a[2] | a[1] | a[0]) == 0 || (b[2] | b[1] | b[0]) == 0)
        return;
    zzmplshift16x4_(a);
    for(bits = 0; zzmplt4_(b, a) && !(b[2] & (1u<<31)) && bits <= 80; bits++){
        zzmpshl3_(b);
    }
    while(bits){
        if(zzmple4_(b, a)){
            zzmpsub4_(a, b, a);
            out[bits >> 5] |= 1 << (bits & 31);
        }
        zzmpshr4_(b);
        bits--;
    }
    if(neg)
        zzmpneg4_(out);
}

/*****************************************************************************/

void zzmpdiv3_(const unsigned a_[3], const unsigned b_[3], unsigned out[4]){
    int bits, neg;
    unsigned a[4], b[4];
    a[0] = a_[0];
    a[1] = a_[1];
    a[2] = a_[2];
    a[3] = 0;
    
    b[0] = b_[0];
    b[1] = b_[1];
    b[2] = b_[2];
    b[3] = 0;
    
    out[0] = out[1] = out[2] = out[3] = 0;
    if((a[2] | a[1] | a[0]) == 0 || (b[2] | b[1] | b[0]) == 0)
        return;
    
    neg = 0;
    if(zzmpabs3_(a))
        neg = !neg;
    if(zzmpabs3_(b))
        neg = !neg;
    zzmplshift16x4_(a);
    /* TODO: a BSR could detect the max bits, instead of the fixed 96+16 and 
     * MSB check on B. */
    for(bits = 0; zzmplt4_(b, a); bits++){
        zzmpshl4_(b);
    }
    assert(bits <= 81);
    assert((b[3] & 0xFFFE0000) == 0); /* We expanded from 96 bits. */
    do{
        /*
        printf("%02i a={%08X%08X%08X%08X}, b={%08X%08X%08X%08X}, %c\n",
            bits,
            a[3], a[2], a[1], a[0],
            b[3], b[2], b[1], b[0],
            zzmple4_(b, a) ? '<' : '>');
         */
        if(zzmple4_(b, a)){
            zzmpsub4_(a, b, a);
            out[bits >> 5] |= 1 << (bits & 31);
        }
        zzmpshr4_(b);
    }while(bits--);
    if(neg)
        zzmpneg4_(out);
}

/*****************************************************************************/

void zzmpdiv4_(const unsigned a_[4], const unsigned b_[4], unsigned out[4]){
    int bits, neg;
    unsigned a[4], b[4];
    a[0] = a_[0];
    a[1] = a_[1];
    a[2] = a_[2];
    a[3] = a_[3];
    
    b[0] = b_[0];
    b[1] = b_[1];
    b[2] = b_[2];
    b[3] = b_[3];
    neg = 0;
    if(zzmpabs4_(a))
        neg = !neg;
    if(zzmpabs4_(b))
        neg = !neg;
    
    out[0] = out[1] = out[2] = out[3] = 0;
    if((a[3] | a[2] | a[1] | a[0]) == 0 || (b[3] | b[2] | b[1] | b[0]) == 0)
        return;
    /* We need to adjust things so the result is shifted.
     * Avoid shifting out the fractional part of the denominator, unless
     * we can't shift the numerator any further.
     */
    if(a[3] <= 0xFFFF){
        /* This is the optimal case. */
        zzmplshift16x4_(a);
    }
    else{
        bits = 126 - zzmpbsr4_(a);
        assert(bits < 16 && bits > -2);
        if(bits < 0){
            zzmprshift4_(b, 16);
        }
        else{
            zzmplshift4_(a, bits);
            zzmprshift4_(b, 16 - bits);
        }
    }
        
    assert((b[3] & (1u<<31)) == 0); /* Should be cleared because of ABS. */
    /* TODO: a BSR could detect the max bits, instead of the fixed 96+16 and 
     * MSB check on B. */
    for(bits = 0; (b[3] & (1u<<30)) == 0 && zzmplt4_(b, a); bits++){
        zzmpshl4_(b);
    }
    assert(bits < 127);
    do{
        /*
        printf("%02i a={%08X%08X%08X%08X}, b={%08X%08X%08X%08X}, %c\n",
            bits,
            a[3], a[2], a[1], a[0],
            b[3], b[2], b[1], b[0],
            zzmple4_(b, a) ? '<' : '>');
         */
        if(zzmple4_(b, a)){
            zzmpsub4_(a, b, a);
            out[bits >> 5] |= 1 << (bits & 31);
        }
        zzmpshr4_(b);
    }while(bits--);
    if(neg)
        zzmpneg4_(out);
}

/*****************************************************************************/

