#ifndef ZZGRAV_MP_H
#define ZZGRAV_MP_H
#pragma once

/*****************************************************************************/
/* ZZ extra precision library.
 * This has 64, 96, and 128 bit arithmetic and bitwise operations.
 * All numbers represent fixed-point values in 48.16, 80.16, and 102.16 form.
 *
 * All functions that take multiple array arguments can have any arguments be
 * the same memory location as any other argument, but it is undefined for
 * the arguments to overlap but not be equal.
 * For instance:
```
// Valid:
unsigned a[3], b[3];
zzmpsub3_(a, b, a); // a is operand 1 and the output.

// Invalid:
unsigned c[4], d[3];
zzmpsub3_(c + 0, d, c + 1); // c is oeprand 1 and the output, but offset.
```
 */

/*
 * Portability:
 *
 * The library requires a C89 or C99 compiler and an appropriate assembler.
 * Current supported compilers + assemblers based on platform are:
 *
 * x86
 *      OpenWatcom C 1.9 + nasm 2.02rc on Windows 2000
 *      OpenWatcom C 1.9 + yasm 1.3.0 on Windows 10
 *      OpenWatcom C 1.9 + yasm 1.3.0 on Linux
 *      GCC 8.5.0 + yasm 1.3.0 on Linux and Windows 10
 *      Clang 10 + yasm 1.3.0 on Linux
 *      MSVC 5.0 + nasm 2.02rc on Windows 2000
 *      MSVC 2015 + yasm 1.3.0 on Windows 10
 *
 * amd64
 *      GCC 8.5.0 + GNU binutils 2.17 on Linux, Haiku, OpenBSD, and Windows 10
 *      GCC 2.95 + GNU binutils 2.17 on Linux and Haiku
 *      GCC 10.0.12 + GNU binutils 2.38 on Linux, Haiku, and OpenBSD
 *      Clang 10 + GNU binutils 2.38 on Linux and OpenBSD
 *      Solaris Studio 11.1 on Linux
 *      GCC 4.6 + GNU assembler on Android NDK r9d
 *      MSVC 2015 + MASM on Windows 10
 *
 * arm32 (armv6 VFP and arm7)
 *      GCC 8.5.0 + GNU binutils 2.38 on Linux and NetBSD
 *      GCC 4.9 + GNU assembler on Android NDK r10e
 *      Clang 10 + GNU binutils 2.29 on Linux and NetBSD
 *
 * aarch64
 *      GCC 8.5.0 + GNU binutils 2.38 on Linux and NetBSD
 *      GCC 4.9 + GNU assembler on Android NDK r10e
 *      Clang 10 + GNU binutils 2.29 on Linux and NetBSD
 *
 * There are experimental UltraSparc and SuperH versions, but they are not
 * mainlined yet. Any additional ports are welcome.
 *
 * Default implementations of extended precision division and square-roots are
 * provided in the "none" directory. These may depend on the ASM functions.
 *
 * Note that the ASM functions might *not* necessarily have unique addresses.
 */

#ifdef __cplusplus
extern "C"{
#endif

#ifdef __DMC__
# define ZZMPFN(T) T cdecl
#elif (defined __WATCOMC__) || (defined __BORLANDC__)
# ifdef _M_IX86
#   define ZZMPFN(T) T _fastcall
# else
#   define ZZMPFN(T) T _cdecl
# endif
#elif (defined _MSC_VER)
# ifdef _M_IX86
#   define ZZMPFN(T) T fastcall
# else
#   define ZZMPFN(T) __declspec(noalias) T cdecl
# endif
#elif (defined __CYGWIN__)
/* fastcall isn't quite compatible with MSVC/Watcom/Borland here :( */
# define ZZMPFN(T) __attribute__((cdecl)) T
# define ZZMPPUREFN(T) __attribute__((pure, cdecl)) T
#else
# define ZZMPFN(T) T
#endif

#ifndef ZZMPPUREFN
# define ZZMPPUREFN ZZMPFN
#endif

/*****************************************************************************/
/* Sign-extend */
ZZMPFN(void) zzmp2s3_(const unsigned n[2], unsigned to[3]);
ZZMPFN(void) zzmp2s4_(const unsigned n[2], unsigned to[4]);
ZZMPFN(void) zzmp3s4_(const unsigned n[3], unsigned to[4]);

/*****************************************************************************/
/* out = a - b */
ZZMPFN(void) zzmpsub2_(const unsigned a[2], const unsigned b[2], unsigned out[3]);
ZZMPFN(void) zzmpsub3_(const unsigned a[3], const unsigned b[3], unsigned out[4]);
ZZMPFN(void) zzmpsub4_(const unsigned a[4], const unsigned b[4], unsigned out[4]);

/*****************************************************************************/
/* out = a + b */
ZZMPFN(void) zzmpadd2_(const unsigned a[2], const unsigned b[2], unsigned out[3]);
ZZMPFN(void) zzmpadd3_(const unsigned a[3], const unsigned b[3], unsigned out[4]);
ZZMPFN(void) zzmpadd4_(const unsigned a[4], const unsigned b[4], unsigned out[4]);

/*****************************************************************************/
/* out = a * b */
ZZMPFN(void) zzmpmul2_(const unsigned a[2], const unsigned b[2], unsigned out[4]);
ZZMPFN(void) zzmpmul3_(const unsigned a[3], const unsigned b[3], unsigned out[4]);
ZZMPFN(void) zzmpmul4_(const unsigned a[4], const unsigned b[4], unsigned out[4]);

/*****************************************************************************/
/* out = a / b */
ZZMPFN(void) zzmpdiv2_(const unsigned a[2], const unsigned b[2], unsigned out[4]);
ZZMPFN(void) zzmpdiv3_(const unsigned a[3], const unsigned b[3], unsigned out[4]);
ZZMPFN(void) zzmpdiv4_(const unsigned a[4], const unsigned b[4], unsigned out[4]);

/*****************************************************************************/
/* n <<= i */
ZZMPFN(void) zzmplshift2_(unsigned n[2], unsigned i);
ZZMPFN(void) zzmplshift3_(unsigned n[3], unsigned i);
ZZMPFN(void) zzmplshift4_(unsigned n[4], unsigned i);

/*****************************************************************************/
/* n <<= 16 */
ZZMPFN(void) zzmplshift16x2_(unsigned n[2]);
ZZMPFN(void) zzmplshift16x3_(unsigned n[3]);
ZZMPFN(void) zzmplshift16x4_(unsigned n[4]);

/*****************************************************************************/
/* n >>= i */
ZZMPFN(void) zzmprshift2_(unsigned n[2], unsigned i);
ZZMPFN(void) zzmprshift3_(unsigned n[3], unsigned i);
ZZMPFN(void) zzmprshift4_(unsigned n[4], unsigned i);

/*****************************************************************************/
/* n <<= 1 */
ZZMPFN(int) zzmpshl2_(unsigned n[2]);
ZZMPFN(int) zzmpshl3_(unsigned n[3]);
ZZMPFN(int) zzmpshl4_(unsigned n[4]);

/*****************************************************************************/
/* n >>= 1 */
ZZMPFN(int) zzmpshr2_(unsigned n[2]);
ZZMPFN(int) zzmpshr3_(unsigned n[3]);
ZZMPFN(int) zzmpshr4_(unsigned n[4]);

/*****************************************************************************/
/* n = -n */
ZZMPFN(int) zzmpneg2_(unsigned n[2]);
ZZMPFN(int) zzmpneg3_(unsigned n[3]);
ZZMPFN(int) zzmpneg4_(unsigned n[4]);

/*****************************************************************************/
/* n = |n|, returns 0 if input is positive. */
ZZMPFN(int) zzmpabs2_(unsigned n[2]);
ZZMPFN(int) zzmpabs3_(unsigned n[3]);
ZZMPFN(int) zzmpabs4_(unsigned n[4]);

/*****************************************************************************/
/* Bit-scan-reverse, return the lowest index with no 1-bits above it. */

ZZMPPUREFN(int) zzmpbsr2_(const unsigned n[2]);
ZZMPPUREFN(int) zzmpbsr3_(const unsigned n[3]);
ZZMPPUREFN(int) zzmpbsr4_(const unsigned n[4]);

/*****************************************************************************/

ZZMPPUREFN(int) zzmplt2_(const unsigned a[2], const unsigned b[2]);
ZZMPPUREFN(int) zzmplt3_(const unsigned a[3], const unsigned b[3]);
ZZMPPUREFN(int) zzmplt4_(const unsigned a[4], const unsigned b[4]);

/*****************************************************************************/

ZZMPPUREFN(int) zzmple2_(const unsigned a[2], const unsigned b[2]);
ZZMPPUREFN(int) zzmple3_(const unsigned a[3], const unsigned b[3]);
ZZMPPUREFN(int) zzmple4_(const unsigned a[4], const unsigned b[4]);

/*****************************************************************************/

ZZMPPUREFN(int) zzmpeq2_(const unsigned a[2], const unsigned b[2]);
ZZMPPUREFN(int) zzmpeq3_(const unsigned a[3], const unsigned b[3]);
ZZMPPUREFN(int) zzmpeq4_(const unsigned a[4], const unsigned b[4]);

/*****************************************************************************/

ZZMPFN(void) zzmpsqrt4_(const unsigned a[4], unsigned out[4]);

/*****************************************************************************/

#ifdef __cplusplus
} // extern "C"
#endif

/*****************************************************************************/

#endif /* ZZGRAV_MP_H */

