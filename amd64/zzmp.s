/* WARNING: For compatibility with the Solaris tools, one line per comment. */
/* AMD64, Unix calling convention + AT&T syntax implementation of */
/* the ZZ extra precision library. */

.text

/* void zzmp2s3_(const unsigned n[2], unsigned to[3]) */
.align 8
.globl zzmp2s3_
zzmp2s3_:
    movl (%rdi), %eax
    movl %eax, (%rsi)
    movslq 4(%rdi), %rax
    movq %rax, 4(%rsi)
    ret

/* void zzmp2s4_(const unsigned n[2], unsigned to[4]) */
.align 8
.globl zzmp2s4_
zzmp2s4_:
    movq (%rdi), %rax
    movq %rax, (%rsi)
    sarq $63, %rax
    movq %rax, 8(%rsi)
    ret

/* void zzmp3s4_(const unsigned n[3], unsigned to[4]) */
.align 8
.globl zzmp3s4_
zzmp3s4_:
    movq (%rdi), %rax
    movq %rax, (%rsi)
    movslq 8(%rdi), %rax
    movq %rax, 8(%rsi)
    ret

/* int zzmpabs2_(unsigned n[2]) */
.align 8
.globl zzmpabs2_
zzmpabs2_:
    movq (%rdi), %rax
	movq %rax, %rdx
    negq %rdx
    cmovns %rdx, %rax
    movq %rax, (%rdi)
    shrq $63, %rax
    ret
    
/* int zzmpneg2_(unsigned n[2]) */
.align 8
.globl zzmpneg2_
zzmpneg2_:
    negq (%rdi)
    ret

/* int zzmpabs3_(unsigned n[3]) */
.align 8
.globl zzmpabs3_
zzmpabs3_:
    movl 8(%rdi), %edx
    testl %edx, %edx
    jns Lzzmpabs3_end_
    negq (%rdi)
    adcl $0, %edx
    negl %edx
    movl %edx, 8(%rdi)
    movl $1, %eax
    ret
Lzzmpabs3_end_:
    xorl %eax, %eax
    ret

/* int zzmpneg3_(unsigned n[3]) */
.align 8
.globl zzmpneg3_
zzmpneg3_:
    negq (%rdi)
    adcl $0, 8(%rdi)
    negl 8(%rdi)
    ret

/* int zzmpabs4_(unsigned n[4]) */
.align 8
.globl zzmpabs4_
zzmpabs4_:
    movq 8(%rdi), %rdx
    testq %rdx, %rdx
    jns Lzzmpabs4_end_
    negq (%rdi)
    adcq %rax, %rdx
    negq %rdx
    movq %rdx, 8(%rdi)
    movl $1, %eax
    ret
Lzzmpabs4_end_:
    xorl %eax, %eax
    ret

/* int zzmpneg4_(unsigned n[4]) */
.align 8
.globl zzmpneg4_
zzmpneg4_:
    negq (%rdi)
    adcq $0, 8(%rdi)
    negq 8(%rdi)
    ret

/* zzmpsub2_(unsigned a[2], unsigned b[2], unsigned out[3]) */
.align 8
.globl zzmpsub2_
zzmpsub2_:
	movl (%rdi), %eax
    subl (%rsi), %eax
    movl %eax, (%rdx)
    movslq 4(%rdi), %rax
    movslq 4(%rsi), %rcx
    sbbq %rcx, %rax
    movq %rax, 4(%rdx)
    ret

/* zzmpsub3_(unsigned a[3], unsigned b[3], unsigned out[4]) */
.align 8
.globl zzmpsub3_
zzmpsub3_:
	movq (%rdi), %rax
    subq (%rsi), %rax
    movq %rax, (%rdx)
    movslq 8(%rdi), %rax
    movslq 8(%rsi), %rcx
    sbbq %rcx, %rax
    movq %rax, 8(%rdx)
    ret

/* zzmpsub4_(unsigned a[4], unsigned b[4], unsigned out[4]) */
.align 8
.globl zzmpsub4_
zzmpsub4_:
	movq (%rdi), %rax
    subq (%rsi), %rax
    movq 8(%rdi), %rcx
    sbbq 8(%rsi), %rcx
    movq %rax, (%rdx)
    movq %rcx, 8(%rdx)
    ret

/* zzmpadd2_(unsigned a[2], unsigned b[2], unsigned out[3]) */
.align 8
.globl zzmpadd2_
zzmpadd2_:
	movl (%rdi), %eax
    addl (%rsi), %eax
    movl %eax, (%rdx)
    movslq 4(%rdi), %rax
    movslq 4(%rsi), %rcx
    adcq %rcx, %rax
    movq %rax, 4(%rdx)
    ret

/* zzmpadd3_(unsigned a[3], unsigned b[3], unsigned out[4]) */
.align 8
.globl zzmpadd3_
zzmpadd3_:
	movq (%rdi), %rax
    addq (%rsi), %rax
    movq %rax, (%rdx)
    movslq 8(%rdi), %rax
    movslq 8(%rsi), %rcx
    adcq %rcx, %rax
    movq %rax, 8(%rdx)
    ret

/* zzmpadd4_(unsigned a[4], unsigned b[4], unsigned out[4]) */
.align 8
.globl zzmpadd4_
zzmpadd4_:
	movq (%rdi), %rax
    addq (%rsi), %rax
    movq 8(%rdi), %rcx
    adcq 8(%rsi), %rcx
    movq %rax, (%rdx)
    movq %rcx, 8(%rdx)
    ret

/* zzmpmul2_(unsigned a[2], unsigned b[2], unsigned out[4]) */
.align 8
.globl zzmpmul2_
zzmpmul2_:
    movq (%rdi), %rax
    movq %rdx, %rdi
    imulq (%rsi)
    shrq $16, %rax
    movq %rax, (%rdi)
    movq %rdx, 6(%rdi)
    sarq $63, %rdx
    movw %dx, 14(%rdi)
    ret

/* zzmpmul4_(unsigned a[3], unsigned b[3], unsigned out[4]) */
.align 8
.globl zzmpmul4_
zzmpmul4_:
    push %rbp
    movq %rsp, %rbp
    
    movq %rdx, %r8
    /* r11 = alo */
    /* rcx = blo */
    /* rdi = ahi */
    /* rsi = bhi */
    movq (%rsi), %rcx
    movq (%rdi), %r11
    movq 8(%rsi), %rsi
    movq 8(%rdi), %rdi
    
    jmp Lmulfull

/* zzmpmul3_(unsigned a[3], unsigned b[3], unsigned out[4]) */
.align 8
.globl zzmpmul3_
zzmpmul3_:
    push %rbp
    movq %rsp, %rbp
    
    movq %rdx, %r8
    /* r11 = alo */
    /* rcx = blo */
    /* rdi = ahi */
    /* rsi = bhi */
    movq (%rsi), %rcx
    movq (%rdi), %r11
    movslq 8(%rsi), %rsi
    movslq 8(%rdi), %rdi
    
Lmulfull:
    testq %rdi, %rdi
    sets %al
    jns Lmpmul_apos
    
    negq %r11
    adcq $0, %rdi
    negq %rdi
    
Lmpmul_apos:
    testq %rsi, %rsi
    sets %dl
    jns Lmpmul_bpos
    
    negq %rcx
    adcq $0, %rsi
    negq %rsi
    
Lmpmul_bpos:
    xorb %al, %dl
    movb %dl, -8(%rbp) /* Save negation flag. */
    
    /* tmp in (low to high): --, r9, r10 */
    
    /* Compute lo * lo, this fully overwrites the low bytes and r9. */
    movq %r11, %rax
    mulq %rcx
    shrq $16, %rax
    movq %rax, (%r8)
    movq %rdx, %r9
    
    /* Compute hi * hi, this fully overwrites r10 */
    movq %rdi, %rax
    mulq %rsi
    movq %rax, %r10
    
    /* Compute hi * lo */
    movq %rdi, %rax
    mulq %rcx
    addq %rax, %r9
    adcq %rdx, %r10
    
    /* Compute lo * hi */
    movq %r11, %rax
    mulq %rsi
    addq %rax, %r9
    adcq %rdx, %r10
    
    movq %r9, 6(%r8)
    movw %r10w, 14(%r8)
    
    testb $1, -8(%rbp)
    jz Lmul_not_negated
    
    negq (%r8)
    adcq $0, 8(%r8)
    negq 8(%r8)
    
Lmul_not_negated:
    leave
    ret

/* void zzmplshift16x2_(unsigned n[2]) */
.align 8
.globl zzmplshift16x2_
zzmplshift16x2_:
    shlq $16, (%rdi)
    ret

/* void zzmplshift16x3_(unsigned n[3]) */
.align 8
.globl zzmplshift16x3_
zzmplshift16x3_:
    movl 6(%rdi), %eax
    shlq $16, (%rdi)
    movl %eax, 8(%rdi)
    ret

/* void zzmplshift16x4_(unsigned n[4]) */
.align 8
.globl zzmplshift16x4_
zzmplshift16x4_:
    movq 6(%rdi), %rax
    shlq $16, (%rdi)
    movq %rax, 8(%rdi)
    ret

/* void zzmplshift2_(unsigned n[2], unsigned i) */
.align 8
.globl zzmplshift2_
zzmplshift2_:
    movl %esi, %ecx
    shlq %cl, (%rdi)
    ret

/* void zzmplshift3_(unsigned n[3], unsigned i) */
.align 8
.globl zzmplshift3_
zzmplshift3_:
    cmpl $1, %esi
    jb Llshift3_end
    movl %esi, %ecx
    je zzmpshl3_
    cmpb $32, %cl
    jge Llshift3_32
    movl (%rdi), %eax
    shll %cl, (%rdi)
    shlq %cl, 4(%rdi)
    movl $32, %ecx
    subl %esi, %ecx
    shrl %cl, %eax
    orl %eax, 4(%rdi)
Llshift3_end:
    ret
.align 8
Llshift3_32:
    xorl %eax, %eax
    subb $32, %cl
    xchgq (%rdi), %rax
    shlq %cl, %rax
    mov %rax, 4(%rdi)
    ret

/* void zzmplshift4_(unsigned n[4], unsigned i) */
.align 8
.globl zzmplshift4_
zzmplshift4_:
    cmpl $1, %esi
    jb Llshift4_end
    je zzmpshl4_
    movl %esi, %ecx
    movl $64, %edx
    cmpl %edx, %ecx
    jge Llshift4_64
    movq (%rdi), %rax
    shlq %cl, (%rdi)
    shlq %cl, 8(%rdi)
    movl %edx, %ecx
    subl %esi, %ecx
    shrq %cl, %rax
    orq %rax, 8(%rdi)
Llshift4_end:
    ret
.align 8
Llshift4_64:
    xorl %eax, %eax
    subl %edx, %ecx
    xchgq (%rdi), %rax
    shlq %cl, %rax
    mov %rax, 8(%rdi)
    ret

/* void zzmprshift2_(unsigned n[2], unsigned i) */
.align 8
.globl zzmprshift2_
zzmprshift2_:
    movl %esi, %ecx
    shrq %cl, (%rdi)
    ret

/* void zzmprshift3_(unsigned n[3], unsigned i) */
.align 8
.globl zzmprshift3_
zzmprshift3_:
    cmpl $1, %esi
    jb Lrshift3_end
    je zzmpshr3_
    movl $32, %edx
    cmpl %edx, %esi
    jge Lrshift3_32
    movl 8(%rdi), %eax
    shrl %cl, (%rdi)
    shrq %cl, 4(%rdi)
    movl %edx, %ecx
    subl %esi, %ecx
    shll %cl, %eax
    orl %eax, 8(%rdi)
Lrshift3_end:
    ret
.align 8
Lrshift3_32:
    xorl %eax, %eax
    subl %edx, %ecx
    xchgq 8(%rdi), %rax
    shrq %cl, %rax
    mov %rax, (%rdi)
    ret

/* void zzmprshift4_(unsigned n[4], unsigned i) */
.align 8
.globl zzmprshift4_
zzmprshift4_:
    cmpl $1, %esi
    jb Lrshift4_end
    je zzmpshr3_
    movl %esi, %ecx
    movl $64, %edx
    cmpl %edx, %esi
    jge Lrshift4_64
    movq 8(%rdi), %rax
    shrq %cl, (%rdi)
    shrq %cl, 8(%rdi)
    movl %edx, %ecx
    subl %esi, %ecx
    shlq %cl, %rax
    orq %rax, (%rdi)
Lrshift4_end:
    ret
.align 8
Lrshift4_64:
    xorl %eax, %eax
    subl %edx, %ecx
    xchgq 8(%rdi), %rax
    shrq %cl, %rax
    mov %rax, (%rdi)
    ret

/* zzmpshl2_(unsigned n[2]) */
.align 8
.globl zzmpshl2_
zzmpshl2_:
    xorl %eax, %eax
    shlq $1, (%rdi)
    setc %al
    ret

/* zzmpshl3_(unsigned n[3]) */
.align 8
.globl zzmpshl3_
zzmpshl3_:
    xorl %eax, %eax
    shlq $1, (%rdi)
    rcll $1, 8(%rdi)
    setc %al
    ret

/* zzmpshl4_(unsigned n[4]) */
.align 8
.globl zzmpshl4_
zzmpshl4_:
    xorl %eax, %eax
    shlq $1, (%rdi)
    rclq $1, 8(%rdi)
    setc %al
    ret

/* zzmpshr2_(unsigned n[2]) */
.align 8
.globl zzmpshr2_
zzmpshr2_:
    shrq $1, (%rdi)
    setc %al
    setc %al
    movzbl %al, %eax
    ret

/* zzmpshr3_(unsigned n[3]) */
.align 8
.globl zzmpshr3_
zzmpshr3_:
    shrl $1, 8(%rdi)
    rcrq $1, (%rdi)
    setc %al
    movzbl %al, %eax
    ret

/* zzmpshr4_(unsigned n[4]) */
.align 8
.globl zzmpshr4_
zzmpshr4_:
    shrq $1, 8(%rdi)
    rcrq $1, (%rdi)
    setc %al
    movzbl %al, %eax
    ret

/* zzmpbsr4_(const unsigned n[4]) */
.align 8
.globl zzmpbsr4_
zzmpbsr4_:
    bsrq 8(%rdi), %rax
    jz zzmpbsr2_
    addl $64, %eax
    ret

/* zzmpbsr2_(const unsigned n[2]) */
.align 8
.globl zzmpbsr2_
zzmpbsr2_:
    xorl %ecx, %ecx
    bsrq (%rdi), %rax
    cmovzl %ecx, %eax
    ret

/* zzmpbsr3_(const unsigned n[3]) */
.align 8
.globl zzmpbsr3_
zzmpbsr3_:
    bsrl 8(%rdi), %eax
    jz zzmpbsr2_
    addl $64, %eax
    ret

/* int zzmplt2_(const unsigned a[2], const unsigned b[2]) */
.align 8
.globl zzmplt2_
zzmplt2_:
    movq (%rdi), %rax
    cmpq (%rsi), %rax
    setl %al
    movzbl %al, %eax
    ret

/* int zzmplt3_(const unsigned a[3], const unsigned b[3]) */
.align 8
.globl zzmplt3_
zzmplt3_:
    movl 8(%rdi), %eax
    movslq %eax, %rcx
    cmpl 8(%rsi), %eax
    je Llt_no
    
Llt_ext:
    xorl %edx, %edx /* Micro-optimization */
    movq (%rdi), %rax
    cmpq (%rsi), %rax
    /* Be absolutely sure to use UNSIGNED comparison here! */
    setb %al
    setae %dl
    testq %rcx, %rcx
    cmovsl %edx, %eax
    movzbl %al, %eax
    ret

/* int zzmplt4_(const unsigned a[4], const unsigned b[4]) */
.align 8
.globl zzmplt4_
zzmplt4_:
    movq 8(%rdi), %rax
    movq %rax, %rcx
    cmpq 8(%rsi), %rax
    je Llt_ext
Llt_no:
    setl %al
    movzbl %al, %eax
    ret

/* int zzmpet2_(const unsigned a[2], const unsigned b[2]) */
.align 8
.globl zzmple2_
zzmple2_:
    movq (%rdi), %rax
    cmpq (%rsi), %rax
    setle %al
    movzbl %al, %eax
    ret

/* int zzmple3_(const unsigned a[3], const unsigned b[3]) */
.align 8
.globl zzmple3_
zzmple3_:
    movslq 8(%rdi), %rcx
    cmpl 8(%rsi), %ecx
    jne Lle_no
Lle_ext:
    xorl %edx, %edx /* Micro-optimization */
    movq (%rdi), %rax
    cmpq (%rsi), %rax
    /* Be absolutely sure to use UNSIGNED comparison here! */
    setbe %al
    setae %dl
    testq %rcx, %rcx
    cmovsl %edx, %eax
    movzbl %al, %eax
    ret

/* int zzmple4_(const unsigned a[4], const unsigned b[4]) */
.align 8
.globl zzmple4_
zzmple4_:
    movq 8(%rdi), %rcx
    cmpq 8(%rsi), %rcx
    je Lle_ext
Lle_no:
    setl %al
    movzbl %al, %eax
    ret

/* int zzmpeq2_(const unsigned a[2], const unsigned b[2]) */
.align 8
.globl zzmpeq2_
zzmpeq2_:
    movq (%rdi), %rax
    cmpq (%rsi), %rax
    sete %al
    movzbl %al, %eax
    ret

/* int zzmpeq3_(const unsigned a[3], const unsigned b[3]) */
.align 8
.globl zzmpeq3_
zzmpeq3_:
    movq (%rdi), %rdx
    cmpq (%rsi), %rdx
    sete %dl
    
    movl 8(%rdi), %eax
    cmpl 8(%rsi), %eax
    movb $0, %al
    cmovnel %edx, %eax
    movzbl %al, %eax
    ret

/* int zzmpeq4_(const unsigned a[4], const unsigned b[4]) */
.align 8
.globl zzmpeq4_
zzmpeq4_:
    movq (%rdi), %rdx
    cmpq (%rsi), %rdx
    sete %dl
    
    movq 8(%rdi), %rax
    cmpq 8(%rsi), %rax
    movb $0, %al
    cmovnel %edx, %eax
    movzbl %al, %eax
    ret

