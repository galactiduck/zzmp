; YASM/NASM implementation of ZZMP for Unix/Windows
; Requires only MMX.

section .text

global zzmp2s3_
global _zzmp2s3_
zzmp2s3_:
_zzmp2s3_:
    mov ecx, [esp+4]
    mov edx, [esp+8]
global $@zzmp2s3_@8
$@zzmp2s3_@8:
    mov eax, [ecx]
    mov [edx], eax
    mov eax [ecx+4]
    mov [edx+4], eax
    sar eax, 31
    mov [edx+8], eax
    ret

global zzmp2s4_
global _zzmp2s4_
zzmp2s4_:
_zzmp2s4_:
    mov ecx, [esp+4]
    mov edx, [esp+8]
global $@zzmp2s4_@8
$@zzmp2s4_@8:
    mov eax, [ecx]
    mov [edx], eax
    mov eax [ecx+4]
    mov [edx+4], eax
    sar eax, 31
    mov [edx+8], eax
    mov [edx+12], eax
    ret

global zzmp3s4_
global _zzmp3s4_
zzmp3s4_:
_zzmp3s4_:
    mov ecx, [esp+4]
    mov edx, [esp+8]
global $@zzmp3s4_@8
$@zzmp3s4_@8:
    mov eax, [ecx]
    mov [edx], eax
    mov eax [ecx+4]
    mov [edx+4], eax
    mov eax [ecx+8]
    mov [edx+8], eax
    sar eax, 31
    mov [edx+12], eax
    ret

; zzmpsub2_(const unsigned a[2], const unsigned b[2], unsigned out[3])
global zzmpsub2_
global _zzmpsub2_
zzmpsub2_:
_zzmpsub2_:
    push ebx
    mov ecx, [esp+8]
    mov edx, [esp+12]
    mov ebx, [esp+16]
    mov eax, [ecx]
    sub eax, [edx]
    mov [ebx], eax
    mov eax, [ecx+4]
    sbb eax, [edx+4]
    mov [ebx+4], eax
    sbb eax, eax
    mov [ebx+8], eax
    pop ebx
    ret
    
global $@zzmpsub2_@12
$@zzmpsub2_@12:
    push ebx
    mov ebx, [esp+8]
    mov eax, [ecx]
    sub eax, [edx]
    mov [ebx], eax
    mov eax, [ecx+4]
    sbb eax, [edx+4]
    mov [ebx+4], eax
    sbb eax, eax
    mov [ebx+8], eax
    pop ebx
    ret 4

; zzmpsub3_(const unsigned a[3], const unsigned b[3], unsigned out[4])
global zzmpsub3_
global _zzmpsub3_
zzmpsub3_:
_zzmpsub3_:
    push ebx
    mov ecx, [esp+8]
    mov edx, [esp+12]
    mov ebx, [esp+16]
    mov eax, [ecx]
    sub eax, [edx]
    mov [ebx], eax
    mov eax, [ecx+4]
    sbb eax, [edx+4]
    mov [ebx+4], eax
    mov eax, [ecx+8]
    sbb eax, [edx+8]
    mov [ebx+8], eax
    sbb eax, eax
    mov [ebx+12], eax
    pop ebx
    ret
    
global $@zzmpsub3_@12
$@zzmpsub3_@12:
    push ebx
    mov ebx, [esp+8]
    mov eax, [ecx]
    sub eax, [edx]
    mov [ebx], eax
    mov eax, [ecx+4]
    sbb eax, [edx+4]
    mov [ebx+4], eax
    mov eax, [ecx+8]
    sbb eax, [edx+8]
    mov [ebx+8], eax
    sbb eax, eax
    mov [ebx+12], eax
    pop ebx
    ret 4

; zzmpsub4_(const unsigned a[4], const unsigned b[4], unsigned out[4])
global _zzmpsub4_
zzmpsub4_:
_zzmpsub4_:
    push edi
    mov ecx, [esp+8]
    mov edx, [esp+12]
    mov edi, [esp+16]
    mov eax, [ecx]
    sub eax, [edx]
    stosd
    mov eax, [ecx+4]
    sbb eax, [edx+4]
    stosd
    mov eax, [ecx+8]
    sbb eax, [edx+8]
    stosd
    mov eax, [ecx+12]
    sbb eax, [edx+12]
    stosd
    sbb eax, eax
    mov [edi], eax
    pop edi
    ret

global $@zzmpsub4_@12
$@zzmpsub4_@12:
    push edi
    mov edi, [esp+8]
    mov eax, [ecx]
    sub eax, [edx]
    stosd
    mov eax, [ecx+4]
    sbb eax, [edx+4]
    stosd
    mov eax, [ecx+8]
    sbb eax, [edx+8]
    stosd
    mov eax, [ecx+12]
    sbb eax, [edx+12]
    stosd
    sbb eax, eax
    mov [edi], eax
    pop edi
    ret 4

; zzmpadd2_(const unsigned a[2], const unsigned b[2], unsigned out[3])
global zzmpadd2_
global _zzmpadd2_
zzmpadd2_:
_zzmpadd2_:
    push ebx
    mov ecx, [esp+8]
    mov edx, [esp+12]
    mov ebx, [esp+16]
    mov eax, [ecx]
    add eax, [edx]
    mov [ebx], eax
    mov eax, [ecx+4]
    adc eax, [edx+4]
    mov [ebx+4], eax
    setc al
    movzx eax, al
    mov [ebx+8], eax
    pop ebx
    ret

global $@zzmpadd2_@12
$@zzmpadd2_@12:
    push ebx
    mov ebx, [esp+8]
    mov eax, [ecx]
    add eax, [edx]
    mov [ebx], eax
    mov eax, [ecx+4]
    adc eax, [edx+4]
    mov [ebx+4], eax
    setc al
    movzx eax, al
    mov [ebx+8], eax
    pop ebx
    ret 4

; zzmpadd3_(const unsigned a[3], const unsigned b[3], unsigned out[4])
global zzmpadd3_
global _zzmpadd3_
zzmpadd3_:
_zzmpadd3_:
    push ebx
    mov ecx, [esp+8]
    mov edx, [esp+12]
    mov ebx, [esp+16]
    mov eax, [ecx]
    add eax, [edx]
    mov [ebx], eax
    mov eax, [ecx+4]
    adc eax, [edx+4]
    mov [ebx+4], eax
    mov eax, [ecx+8]
    adc eax, [edx+8]
    mov [ebx+8], eax
    setc al
    movzx eax, al
    mov [ebx+12], eax
    pop ebx
    ret

global $@zzmpadd3_@12
$@zzmpadd3_@12:
    push ebx
    mov ebx, [esp+8]
    mov eax, [ecx]
    add eax, [edx]
    mov [ebx], eax
    mov eax, [ecx+4]
    adc eax, [edx+4]
    mov [ebx+4], eax
    mov eax, [ecx+8]
    adc eax, [edx+8]
    mov [ebx+8], eax
    setc al
    movzx eax, al
    mov [ebx+12], eax
    pop ebx
    ret 4

; zzmpadd4_(const unsigned a[4], const unsigned b[4], unsigned out[4])
global zzmpadd4_
global _zzmpadd4_
zzmpadd4_:
_zzmpadd4_:
    push edi
    mov ecx, [esp+8]
    mov edx, [esp+12]
    mov edi, [esp+16]
    mov eax, [ecx]
    add eax, [edx]
    stosd
    mov eax, [ecx+4]
    adc eax, [edx+4]
    stosd
    mov eax, [ecx+8]
    adc eax, [edx+8]
    stosd
    mov eax, [ecx+12]
    adc eax, [edx+12]
    stosd
    setc al
    movzx eax, al
    mov [edi], eax
    pop edi
    ret

global $@zzmpadd4_@12
$@zzmpadd4_@12:
    push edi
    mov edi, [esp+8]
    mov eax, [ecx]
    sub eax, [edx]
    stosd
    mov eax, [ecx+4]
    sbb eax, [edx+4]
    stosd
    mov eax, [ecx+8]
    sbb eax, [edx+8]
    stosd
    mov eax, [ecx+12]
    sbb eax, [edx+12]
    stosd
    setc al
    movzx eax, al
    mov [edi], eax
    pop edi
    ret 4

global zzmpshr2_
global _zzmpshr2_
zzmpshr2_:
_zzmpshr2_:
    mov edx, [esp+4]
Lmpshr2:
    shr [edx], 1
    rcr [edx+4], 1
    ret

global $@zzmpshr2_@8
$@zzmpshr2_@8:
    shr [ecx], 1
    rcr [ecx+4], 1
    ret

global zzmplshift2_
global _zzmplshift2_
zzmplshift2_:
_zzmplshift2_:
    mov edx, [esp+4]
    mov ecx, [esp+8]
    cmp cl, 1
    je Lmpshr2
    jl .end
    mov eax, [edx]
    shl DWORD [edx], cl
    shl DWORD [edx+4], cl
    sub cl, 32
    neg cl
    shr eax, cl
    or [edx+4], eax
.end:
    ret
    
global $@zzmplshift2_@8
$@zzmplshift2_@8:
    cmp dl, 1
    jl .end
    xchg ecx, edx
    je Lmpshr2
    mov eax, [edx]
    shl DWORD [edx], cl
    shl DWORD [edx+8], cl
    sub cl, 32
    neg cl
    shr eax, cl
    or [edx+8], eax
.end:
    ret

global zzmpshr3_
global _zzmpshr3_
zzmpshr3_:
_zzmpshr3_:
    mov edx, [esp+4]
Lmpshr3:
    shr [edx], 1
    rcr [edx+4], 1
    rcr [edx+8], 1
    ret

global $@zzmpshr3_@8
$@zzmpshr3_@8:
    shr [ecx], 1
    rcr [ecx+4], 1
    rcr [ecx+8], 1
    ret

global zzmplshift3_
global _zzmplshift3_
zzmplshift3_:
_zzmplshift3_:
    mov edx, [esp+4]
    mov ecx, [esp+8]
    cmp cl, 1
    je Lmpshr3
    jl .end
Lmplshift3:
    push ebx
    mov eax, [edx]
    mov ebx, [edx+4]
    shl DWORD [edx], cl
    shl DWORD [edx+4], cl
    shl DWORD [edx+8], cl
    sub cl, 32
    neg cl
    shr eax, cl
    shr ebx, cl
    or [edx+4], eax
    or [edx+8], ebx
    pop ebx
.end:
    ret

global $@zzmplshift3_@8
$@zzmplshift3_@8:
    xchg ecx, edx
    cmp cl, 1
    jg Lmplshift3
    je Lmpshr3
    ret

global zzmpshr4_
global _zzmpshr4_
zzmpshr4_:
_zzmpshr4_:
    mov edx, [esp+4]
Lmpshr4:
    shr [edx], 1
    rcr [edx+4], 1
    rcr [edx+8], 1
    rcr [edx+12], 1
    ret

global $@zzmpshr4_@8
$@zzmpshr4_@8:
    shr [ecx], 1
    rcr [ecx+4], 1
    rcr [ecx+8], 1
    rcr [ecx+12], 1
    ret

global zzmplshift4_
global _zzmplshift4_
zzmplshift4_:
_zzmplshift4_:
    mov edx, [esp+4]
    mov ecx, [esp+8]
    cmp cl, 1
    je Lmpshr4
    jl .end
Lmplshift3:
    push edi
    push ebx
    mov eax, [edx]
    mov ebx, [edx+4]
    mov edi, [edx+8]
    shl DWORD [edx], cl
    shl DWORD [edx+4], cl
    shl DWORD [edx+8], cl
    shl DWORD [edx+12], cl
    sub cl, 32
    neg cl
    shr eax
    shr ebx
    shr edi
    or [edx+4], eax
    or [edx+8], ebx
    pop ebx
    or [edx+12], edi
    pop edi
.end:
    ret
    
global $@zzmplshift4_@8
$@zzmplshift4_@8:
    xchg ecx, edx
    cmp cl, 1
    jg Lmplshift4
    je Lmpshr4
    ret

global zzmplshift16x2_
global _zzmplshift16x2_
zzmplshift16x2_:
_zzmplshift16x2_:
    mov ecx, [esp+4]
global $@zzmplshift16x2_@4
$@zzmplshift16x2_@4:
    mov eax, [ecx]
    mov edx, [ecx+4]
    mov [ecx+2], eax
    mov [ecx+6], dx
    mov WORD [ecx], 0
    ret

global zzmplshift16x3_
global _zzmplshift16x3_
zzmplshift16x3_:
_zzmplshift16x3_:
    mov ecx, [esp+4]
global $@zzmplshift16x3_@4
$@zzmplshift16x3_@4:
    mov eax, [ecx]
    mov edx, [ecx+4]
    mov [ecx+2], eax
    mov eax, [ecx+8]
    mov [ecx+6], edx
    mov [ecx+10], ax
    mov WORD [ecx], 0
    ret

global zzmplshift16x4_
global _zzmplshift16x4_
zzmplshift16x4_:
_zzmplshift16x4_:
    mov eax, edi
    mov edx, esi
    mov esi, [esp+8]
    lea edi, [esi+2]
    mov ecx, 7
    rep movsw
    mov WORD [edi-16], 0
    mov edi, eax
    mov esi, edx
    ret

global $@zzmplshift16x4_@4
$@zzmplshift16x4_@4:
    mov edx, esi
    mov esi, ecx
    lea eax, [esi+2]
    xchg eax, edi
    mov ecx, 7
    rep movsw
    mov WORD [edi-16], 0
    mov edi, eax
    mov esi, edx
    ret

