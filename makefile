
ZZCC?=gcc
ZZCXX?=g++
ZZF77?=gfortran
ZZAR?=ar
ZZRANLIB?=ranlib
ZZASM?=as

CC=$(ZZCC)
CXX=$(ZZCXX)
F77=$(ZZF77)
AR=$(ZZAR)
RANLIB=$(ZZRANLIB)
ASM=$(ZZASM)

# Possible values: amd64 x86 arm arm64 none
ARCH?=amd64
# Possible values: unix win32 solaris
OS?=unix

YASM!=which yasm 2>/dev/null || which nasm 2>/dev/null || echo yasm

EXTFLAGS!=case '$(ARCH)' in \
    amd64) echo ' -fno-plt -march=x86-64 -msse3 -mtune=barcelona' ;; \
    x86) echo ' -fno-plt -march=pentium -mtune=barcelona -m32' ;; \
    arm) echo ' -fPIC -mcpu=armv6z+fp -mfpu=vfpv2 -masm-syntax-unified' ;; \
esac

ZZCCTYPE!="$(CC)" --version | head -n 1 | awk 'BEGIN{I="none"} $$1 ~ /gcc|icc/{I=$$1} $$1 ~ /^(clang|Intel)/{I="clang"} END{print(I)}'

GCCFLAGS=-ffreestanding -nostdlib -funswitch-loops -fira-hoist-pressure -ftree-loop-linear -floop-strip-mine -floop-block
GCCCFLAGS=$(GCCFLAGS) -Wno-long-long
CLANGCFLAGS=-ffreestanding -nostdlib -fblocks -fno-exceptions -fgnu-keywords -fstrict-enums -Wno-long-long
EXTCFLAGS!=case '$(ZZCCTYPE),$(ARCH)' in \
    gcc,x86) echo ' $(GCCCFLAGS) -fira-region=all' ;; \
    gcc,amd64) echo ' $(GCCCFLAGS) -fira-loop-pressure' ;; \
    gcc,*) echo ' $(GCCCFLAGS) ' ;; \
    clang,*) echo ' $(CLANGCFLAGS)' ;; \
esac

CFLAGS=$(EXTFLAGS) $(EXTCFLAGS) -ffast-math -g -c -Wall -Wextra -pedantic -ansi -O2
F77OPTFLAGS=$(GCCFLAGS) -ffast-math -fassociative-math -freciprocal-math -fno-signed-zeros -fno-trapping-math -ffinite-math-only -fversion-loops-for-strides -ffrontend-optimize -faggressive-function-elimination
F77FLAGS=$(EXTFLAGS) -g -std=legacy -fdec-structure -ffixed-form -O3 $(F77OPTFLAGS)
LINKFLAGS=$(EXTFLAGS) -L$(ARCH) -g -fno-plt -O2

all: libzzmp.a

.SUFFIXES:

TESTOBJ=zztest.o
zztest: $(TESTOBJ) $(ARCH)/libzzmp.a
	$(CC) $(LINKFLAGS) -o zztest $(TESTOBJ) -lm -lc -lzzmp

zztest.o: zztest.c yyy_test.h zzmp.h
	$(CC) $(LINKFLAGS) -c -o $@ zztest.c

check: zztest
	exec ./zztest

test: check

libzzmp.a: $(ARCH)/libzzmp.a
	cp $(ARCH)/libzzmp.a $@

none/zzdiv.o: none/zzdiv.c zzmp.h
	$(CC) $(CFLAGS) -I. -c -o $@ none/zzdiv.c

none/zzsqrt.o: none/zzsqrt.c zzmp.h
	$(CC) $(CFLAGS) -I. -c -o $@ none/zzsqrt.c

AMD64OBJ=none/zzdiv.o none/zzsqrt.o amd64/zzmp.o
amd64/libzzmp.a: $(AMD64OBJ)
	$(AR) rc $@ $(AMD64OBJ)
	$(RANLIB) $@

AMD64ASFLAGS=--gdwarf-2 -n --noexecstack -mindex-reg
amd64/zzmp.o: amd64/zzmp.s
	$(ASM) -o $@ -O2 $(AMD64ASFLAGS) amd64/zzmp.s

X86OBJ=zzdiv.o zzsqrt.o x86/zzmp.o
x86/libzzmp.a: $(X86OBJ)
	$(AR) rc $@ $(X86OBJ)
	$(RANLIB) $@

x86/zzmp.o: x86/zzmp.asm
	$(YASM) -gstabs -f elf -o $@ x86/zzmp.asm

clean:
	rm -f amd64/zzmp.o amd64/libzzmp.a x86/zzmp.o x86/libzzmp.a libzzmp.a
	rm -f none/zzdiv.o none/zzsqrt.o zztest.o

.PHONY: clean all check test
.IGNORE: clean

