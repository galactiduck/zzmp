#include "yyy_test.h"
#include "zzmp.h"

#ifdef __SUNPRO_C
/* Not concerned about this. */
# pragma error_messages (off,E_INIT_SIGN_EXTEND)
#endif

/*****************************************************************************/
/* Tests for the ZZMP library, ZZGrav, and the IPC/thread management library.
 *
 * Most of these are contrived examples, or examples determined by guessing at
 * weak points. Any real issues found will be added.
 */

static char *create_error_msg(
    const char *op,
    unsigned n,
    const unsigned *exp, 
    const unsigned *act){
    
    char *msg, buf[64];
    unsigned i;
    msg = malloc(512);
    snprintf(msg, 512, "%s expected {", op);
    for(i = n; i != 0; i--){
        snprintf(buf, sizeof(buf), "%i%c", exp[i-1], i == 1 ? '}' : ',');
        buf[sizeof(buf)-1] = 0;
        strcat(msg, buf);
    }
    strcat(msg, ", actual {");
    for(i = n; i != 0; i--){
        snprintf(buf, sizeof(buf), "%i%c", act[i-1], i == 1 ? '}' : ',');
        buf[sizeof(buf)-1] = 0;
        strcat(msg, buf);
    }
    return msg;
}

/* Helper used to check with each source and dest being equal separately. */
#define TESTBINOP(OP, A, B, CHECK) do{ \
    unsigned a_[4], b_[4], check_[4] = {0, 0, 0, 0}; \
    unsigned i_; \
    char *msg_; \
    memcpy(a_, (A), sizeof(A)); \
    memcpy(b_, (B), sizeof(B)); \
    OP(a_, b_, check_); \
    for(i_ = 0; i_ < sizeof(CHECK)/sizeof(*(CHECK)); i_++){ \
        if(check_[i_] != (CHECK)[i_]){ \
            msg_ = create_error_msg(#OP, \
                sizeof(CHECK)/sizeof(*(CHECK)), (CHECK), check_); \
            YYY_ASSERT_MSG(msg_); \
            free(msg_); \
            SUCCESS_INDICATOR = 0; \
            break; \
        } \
    } \
    if(i_ != sizeof(CHECK)/sizeof(*(CHECK))) break; \
    memcpy(a_, (A), sizeof(A)); \
    memcpy(b_, (B), sizeof(B)); \
    OP(a_, b_, a_); \
    for(i_ = 0; i_ < sizeof(CHECK)/sizeof(*(CHECK)); i_++){ \
        if(a_[i_] != (CHECK)[i_]){ \
            msg_ = create_error_msg(#OP " (out=a)", \
                sizeof(CHECK)/sizeof(*(CHECK)), (CHECK), a_); \
            YYY_ASSERT_MSG(msg_); \
            free(msg_); \
            SUCCESS_INDICATOR = 0; \
            break; \
        } \
    } \
    memcpy(a_, (A), sizeof(A)); \
    memcpy(b_, (B), sizeof(B)); \
    OP(a_, b_, b_); \
    for(i_ = 0; i_ < sizeof(CHECK)/sizeof(*(CHECK)); i_++){ \
        if(b_[i_] != (CHECK)[i_]){ \
            msg_ = create_error_msg(#OP " (out=b)", \
                sizeof(CHECK)/sizeof(*(CHECK)), (CHECK), b_); \
            YYY_ASSERT_MSG(msg_); \
            free(msg_); \
            SUCCESS_INDICATOR = 0; \
            break; \
        } \
    } \
}while(0)

/*****************************************************************************/

static const unsigned all_zero2[2] = {0, 0};
static const unsigned all_zero3[3] = {0, 0, 0};
static const unsigned all_zero4[4] = {0, 0, 0, 0};

/*****************************************************************************/
/* Addition tests. */

static int testadd2_1(void){
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpadd2_, all_zero2, all_zero2, all_zero3);
    return SUCCESS_INDICATOR;
}

static int testadd2_2(void){
    static const unsigned x[2] = {256, 0};
    static const unsigned check[3] = {512, 0, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpadd2_, x, x, check);
    return SUCCESS_INDICATOR;
}

static int testadd2_3(void){
    static const unsigned a[2] = {-256, -1};
    static const unsigned b[2] = {99, -17};
    static const unsigned check[3] = {-157, -18, -1};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpadd2_, a, b, check);
    TESTBINOP(zzmpadd2_, b, a, check);
    return SUCCESS_INDICATOR;
}

static int testadd2_4(void){
    static const unsigned a[2] = {0x8A55BBCC, 312};
    static const unsigned b[2] = {0xEA57D00D, 292};
    static const unsigned check[3] = {0x74AD8BD9, 605, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpadd2_, a, b, check);
    TESTBINOP(zzmpadd2_, b, a, check);
    return SUCCESS_INDICATOR;
}

static int testadd3_1(void){
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpadd3_, all_zero3, all_zero3, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testadd3_2(void){
    static const unsigned x[3] = {0, 256, 0};
    static const unsigned check[4] = {0, 512, 0, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpadd3_, x, x, check);
    return SUCCESS_INDICATOR;
}

static int testadd3_3(void){
    static const unsigned a[3] = {-256, -1, -1};
    static const unsigned b[3] = {99, -17, -1};
    static const unsigned check[4] = {-157, -18, -1, -1};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpadd3_, a, b, check);
    TESTBINOP(zzmpadd3_, b, a, check);
    return SUCCESS_INDICATOR;
}

static int testadd3_4(void){
    static const unsigned a[3] = {0x8A55BBCC, 0xFFFFFFFF, 292};
    static const unsigned b[3] = {0xEA57D00D, 0xFFFFFFFF, 312};
    static const unsigned check[4] = {0x74AD8BD9, 0xFFFFFFFF, 605, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpadd3_, a, b, check);
    TESTBINOP(zzmpadd3_, b, a, check);
    return SUCCESS_INDICATOR;
}

/*****************************************************************************/
/* Subtraction tests. */

static int testsub2_1(void){
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub2_, all_zero2, all_zero2, all_zero3);
    return SUCCESS_INDICATOR;
}

static int testsub2_2(void){
    static const unsigned x[2] = {255, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub2_, x, x, all_zero3);
    return SUCCESS_INDICATOR;
}

static int testsub2_3(void){
    static const unsigned x[2] = {100000, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub2_, x, x, all_zero3);
    return SUCCESS_INDICATOR;
}

static int testsub2_4(void){
    static const unsigned x[2] = {100099, 312292};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub2_, x, x, all_zero3);
    return SUCCESS_INDICATOR;
}

static int testsub2_5(void){
    static const unsigned x[2] = {~0, ~0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub2_, x, x, all_zero3);
    return SUCCESS_INDICATOR;
}

static int testsub2_6(void){
    static const unsigned x[2] = {0x7FFFFFFF, 0x7FFFFFFF};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub2_, x, x, all_zero3);
    return SUCCESS_INDICATOR;
}

static int testsub2_7(void){
    static const unsigned b[2] = {255, 0};
    static const unsigned check[3] = {
        0xFFFFFF01, ~0, ~0
    };
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub2_, all_zero2, b, check);
    return SUCCESS_INDICATOR;
}

static int testsub2_8(void){
    static const unsigned a[2] = {1, 99};
    static const unsigned b[2] = {75611, 31299};
    static const unsigned check[3] = {
        -75610, -31201, ~0
    };
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub2_, a, b, check);
    return SUCCESS_INDICATOR;
}

static int testsub3_1(void){
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub3_, all_zero3, all_zero3, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testsub3_2(void){
    static const unsigned x[3] = {255, 1, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub3_, x, x, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testsub3_3(void){
    static const unsigned x[3] = {100000, 0, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub3_, x, x, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testsub3_4(void){
    static const unsigned x[3] = {100099, 312292, 18};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub3_, x, x, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testsub3_5(void){
    static const unsigned x[3] = {~0, ~0, ~0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub3_, x, x, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testsub3_6(void){
    static const unsigned x[3] = {0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub3_, x, x, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testsub3_7(void){
    static const unsigned b[3] = {255, 0};
    static const unsigned check[4] = {
        0xFFFFFF01, ~0, ~0, ~0
    };
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub3_, all_zero3, b, check);
    return SUCCESS_INDICATOR;
}

static int testsub3_8(void){
    static const unsigned a[3] = {1, 99, 0};
    static const unsigned b[3] = {75611, 31299, 1};
    static const unsigned check[4] = {
        -75610, -31201, ~1, ~0
    };
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpsub3_, a, b, check);
    return SUCCESS_INDICATOR;
}

/*****************************************************************************/
/* Multiplication tests. */

static int testmul2_1(void){
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpmul2_, all_zero2, all_zero2, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testmul2_2(void){
    static const unsigned a[2] = {292, 312};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpmul2_, a, all_zero2, all_zero4);
    TESTBINOP(zzmpmul2_, all_zero2, a, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testmul2_3(void){
    static const unsigned a[2] = {0xF0007001, 292};
    static const unsigned b[2] = {2<<16, 0};
    static const unsigned check[4] = {0xe000e002, 585, 0, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpmul2_, a, b, check);
    TESTBINOP(zzmpmul2_, b, a, check);
    return SUCCESS_INDICATOR;
}

static int testmul2_4(void){
    static const unsigned a[2] = {919384, -721};
    static const unsigned b[2] = {100991, 0};
    static const unsigned check[4] = {4033257027u, -1112, ~0, ~0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpmul2_, a, b, check);
    TESTBINOP(zzmpmul2_, b, a, check);
    return SUCCESS_INDICATOR;
}

static int testmul3_1(void){
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpmul3_, all_zero3, all_zero3, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testmul3_2(void){
    static const unsigned a[3] = {292, 312, -1};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpmul3_, a, all_zero3, all_zero4);
    TESTBINOP(zzmpmul3_, all_zero3, a, all_zero4);
    return SUCCESS_INDICATOR;
}

static int testmul3_3(void){
    static const unsigned a[3] = {0xF0007001, 292, 0};
    static const unsigned b[3] = {2<<16, 0, 0};
    static const unsigned check[4] = {0xe000e002, 585, 0, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpmul3_, a, b, check);
    TESTBINOP(zzmpmul3_, b, a, check);
    return SUCCESS_INDICATOR;
}

static int testmul3_4(void){
    static const unsigned a[3] = {919384, -721, -1};
    static const unsigned b[3] = {100991, 0, 0};
    static const unsigned check[4] = {4033257028u, -1112, ~0, ~0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpmul3_, a, b, check);
    TESTBINOP(zzmpmul3_, b, a, check);
    return SUCCESS_INDICATOR;
}

/*****************************************************************************/

static int testlshift3_1(void){
    int i;
    char *msg;
    unsigned a[3] = {0, 312 << 16, 0};
    static const unsigned check[3] = {0, 0, 312};
    zzmplshift16x3_(a);
    for(i = 0; i < 3; i++){
        if(a[i] != check[i]){
            msg = create_error_msg("zzmplshift16x3_", 3, check, a);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

static int testlshift3_2(void){
    int i;
    char *msg;
    unsigned a[3] = {0x312A292B, 0xCDFEABCD, 0xC0FFEE12};
    static const unsigned check[3] = {0x292B0000, 0xABCD312A, 0xEE12CDFE};
    zzmplshift16x3_(a);
    for(i = 0; i < 3; i++){
        if(a[i] != check[i]){
            msg = create_error_msg("zzmplshift16x3_", 3, check, a);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

static int testshr3_1(void){
    int i;
    char *msg;
    unsigned a[3] = {0x312A292B, 0xCDFEABCD, 0xC0FFEE12};
    static const unsigned check[3] = {0x98951495,0x66FF55E6,0x607FF709};
    zzmpshr3_(a);
    for(i = 0; i < 3; i++){
        if(a[i] != check[i]){
            msg = create_error_msg("zzmpshr3_", 3, check, a);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

static int testshr3_2(void){
    int i;
    char *msg;
    unsigned a[3] = {77, 99, 9999};
    static const unsigned check[3] = {
        (77>>1) | (1u<<31),
        (99>>1) | (1u<<31),
        9999>>1
    };
    zzmpshr3_(a);
    for(i = 0; i < 3; i++){
        if(a[i] != check[i]){
            msg = create_error_msg("zzmpshr3_", 3, check, a);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

static int testshr4_1(void){
    int i;
    char *msg;
    unsigned a[4] = {0x312A292B, 0xCDFEABCD, 0xC0FFEE12, 999999999U};
    static const unsigned check[4] = {
        0x98951495,
        0x66FF55E6,
        0xE07FF709,
        499999999U
    };
    zzmpshr4_(a);
    for(i = 0; i < 4; i++){
        if(a[i] != check[i]){
            msg = create_error_msg("zzmpshr4_", 4, check, a);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

static int testshr4_2(void){
    int i;
    char *msg;
    unsigned a[4] = {77, 99, 1, 9999};
    static const unsigned check[4] = {
        (77>>1) | (1u<<31),
        (99>>1) | (1u<<31),
        (1u<<31),
        9999>>1
    };
    zzmpshr4_(a);
    for(i = 0; i < 4; i++){
        if(a[i] != check[i]){
            msg = create_error_msg("zzmpshr4_", 4, check, a);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

/*****************************************************************************/

static int testdiv3_1(void){
    static const unsigned a[3] = {312 << 16, 0, 0};
    static const unsigned b[3] = {292 << 16, 0, 0};
    static const unsigned check[4] = {70024, 0, 0, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpdiv3_, a, b, check);
    return SUCCESS_INDICATOR;
}

static int testdiv3_2(void){
    static const unsigned a[3] = {0, 312 << 16, 0};
    static const unsigned b[3] = {292 << 16, 0, 0};
    static const unsigned check[4] = {3294769432u, 70024, 0, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpdiv3_, a, b, check);
    return SUCCESS_INDICATOR;
}

static int testdiv3_3(void){
    static const unsigned a[3] = {0, 0, 312 << 16};
    static const unsigned b[3] = {292 << 16, 0, 0};
    static const unsigned check[4] = {2353406737u, 3294769432u, 70024, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpdiv3_, a, b, check);
    return SUCCESS_INDICATOR;
}

static int testdiv3_4(void){
    char buf[80], *msg;
    static const unsigned one[3] = {1<<16, 0, 0};
    unsigned val[4], check[4];
    int SUCCESS_INDICATOR, i, bit;
    SUCCESS_INDICATOR = 1;
    for(bit = 0; bit < 96; bit++){
        check[0] = check[1] = check[2] = check[3] = 0;
        check[bit >> 31] |= 1<<(i & 31);
        zzmpdiv3_(check, one, val);
        for(i = 0; i < 4; i++){
            if(val[i] != check[i]){
                snprintf(buf, sizeof(buf), "zzmpdiv3_ (bit %i)\n", bit);
                msg = create_error_msg(buf, 4, check, val);
                YYY_ASSERT_MSG(msg);
                free(msg);
                return 0;
            }
        }
    }
    return SUCCESS_INDICATOR;
}

static int testdiv4_1(void){
    static const unsigned a[4] = {0xF0669E44, 0xFFFFFBA8, ~0u, 0x000F4240};
    static const unsigned b[4] = {0, 0, 4, 0};
    static const unsigned check[4] = {~0u, 3499114495u, 3, 0};
    int SUCCESS_INDICATOR = 1;
    TESTBINOP(zzmpdiv4_, a, b, check);
    return SUCCESS_INDICATOR;
}

/*****************************************************************************/

static int testdivparts_1(void){
    static const unsigned a[4] = {0, 0, 0, 0x00000138};
    unsigned b[4] = {0, 0, 0, 0x00000248};
    int SUCCESS_INDICATOR = 1;
    zzmpshr4_(b);
    YYY_EXPECT_INT_EQ(b[3], 0x00000124);
    YYY_EXPECT_INT_EQ(zzmple4_(b, a), 1);
    return SUCCESS_INDICATOR;
}

static int testdivparts_2(void){
    unsigned a[4] = {0, 0, 0, 0x00000138};
    unsigned b[4] = {0, 0, 0, 0x00000248};
    int SUCCESS_INDICATOR = 1;
    zzmpshr4_(b);
    YYY_EXPECT_INT_EQ(zzmple4_(b, a), 1);
    zzmpsub4_(a, b, a);
    YYY_EXPECT_INT_EQ(a[3], 0x00000014);
    YYY_EXPECT_INT_EQ(zzmple4_(b, a), 0);
    return SUCCESS_INDICATOR;
}

/*****************************************************************************/

static int testlt4_1(void){
    static const unsigned a[4] = {0, 0, 0, 0x00000138};
    static const unsigned b[4] = {0, 0, 0, 0x00000248};
    int SUCCESS_INDICATOR = 1;
    YYY_EXPECT_INT_EQ(zzmple4_(b, a), 0);
    YYY_EXPECT_INT_EQ(zzmplt4_(a, b), 1);
    return SUCCESS_INDICATOR;
}

static int testlt4_2(void){
    static const unsigned a[4] = {0, 0, 0x1C000000, 0};
    static const unsigned b[4] = {0, 0, 0x92000000, 0};
    int SUCCESS_INDICATOR = 1;
    YYY_EXPECT_INT_EQ(zzmple4_(b, a), 0);
    YYY_EXPECT_INT_EQ(zzmplt4_(b, a), 0);
    return SUCCESS_INDICATOR;
}

static int testlt4_3(void){
    static const unsigned a[4] = {0, 0x1C000000, 0, 0};
    static const unsigned b[4] = {0, 0x92000000, 0, 0};
    int SUCCESS_INDICATOR = 1;
    YYY_EXPECT_INT_EQ(zzmple4_(b, a), 0);
    YYY_EXPECT_INT_EQ(zzmplt4_(b, a), 0);
    return SUCCESS_INDICATOR;
}

/*****************************************************************************/

static int testsqrt4_1(void){
    static const unsigned check[4] = {1<<16, 0, 0, 0};
    unsigned i;
    char *msg;
    unsigned val[4];
    zzmpsqrt4_(check, val);
    for(i = 0; i < 4; i++){
        if(val[i] != check[i]){
            msg = create_error_msg("zzmpsqrt4_", 4, check, val);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

static int testsqrt4_2(void){
    static const unsigned a[4] = {100991, 0, 0, 0};
    static const unsigned check[4] = {81354, 0, 0, 0};
    unsigned val[4];
    unsigned i;
    char *msg;
    zzmpsqrt4_(a, val);
    for(i = 0; i < 4; i++){
        if(val[i] != check[i]){
            msg = create_error_msg("zzmpsqrt4_", 4, check, val);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

static int testsqrt4_3(void){
    static const unsigned a[4] = {4033257028u, -1112, ~0, 1000000};
    /*  This would be the proper result, but we lose some percision :(
    static const unsigned check[4] = {2602336256u, 3892322500u, 3, 0};
    */
    static const unsigned check[4] = {2602332924u, 3892322500u, 3, 0};
    unsigned val[4];
    unsigned i;
    char *msg;
    zzmpsqrt4_(a, val);
    for(i = 0; i < 4; i++){
        if(val[i] != check[i]){
            msg = create_error_msg("zzmpsqrt4_", 4, check, val);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

static int testsqrt4_4(void){
    static const unsigned a[4] = {919384, -721, -1, 0};
    static const unsigned check[4] = {-2, 0x00FFFFFF, 0, 0};
    unsigned val[4];
    unsigned i;
    char *msg;
    zzmpsqrt4_(a, val);
    for(i = 0; i < 4; i++){
        if(val[i] != check[i]){
            msg = create_error_msg("zzmpsqrt4_", 4, check, val);
            YYY_ASSERT_MSG(msg);
            free(msg);
            return 0;
        }
    }
    return 1;
}

/*****************************************************************************/

static struct YYY_Test zzmp_tests[] = {
    YYY_TEST(testadd2_1),
    YYY_TEST(testadd2_2),
    YYY_TEST(testadd2_3),
    YYY_TEST(testadd2_4),
    
    YYY_TEST(testadd3_1),
    YYY_TEST(testadd3_2),
    YYY_TEST(testadd3_3),
    YYY_TEST(testadd3_4),
    
    YYY_TEST(testsub2_1),
    YYY_TEST(testsub2_2),
    YYY_TEST(testsub2_3),
    YYY_TEST(testsub2_4),
    YYY_TEST(testsub2_5),
    YYY_TEST(testsub2_6),
    YYY_TEST(testsub2_7),
    YYY_TEST(testsub2_8),
    
    YYY_TEST(testsub3_1),
    YYY_TEST(testsub3_2),
    YYY_TEST(testsub3_3),
    YYY_TEST(testsub3_4),
    YYY_TEST(testsub3_5),
    YYY_TEST(testsub3_6),
    YYY_TEST(testsub3_7),
    YYY_TEST(testsub3_8),
    
    YYY_TEST(testmul2_1),
    YYY_TEST(testmul2_2),
    YYY_TEST(testmul2_3),
    YYY_TEST(testmul2_4),
    
    YYY_TEST(testmul3_1),
    YYY_TEST(testmul3_2),
    YYY_TEST(testmul3_3),
    YYY_TEST(testmul3_4),
    
    YYY_TEST(testlshift3_1),
    YYY_TEST(testlshift3_2),
    
    YYY_TEST(testshr3_1),
    YYY_TEST(testshr3_2),
    
    YYY_TEST(testshr4_1),
    YYY_TEST(testshr4_2),
    
    YYY_TEST(testdiv3_1),
    YYY_TEST(testdiv3_2),
    YYY_TEST(testdiv3_3),
    YYY_TEST(testdiv3_4),
    
    YYY_TEST(testdiv4_1),
    
    YYY_TEST(testdivparts_1),
    YYY_TEST(testdivparts_2),
    
    YYY_TEST(testlt4_1),
    YYY_TEST(testlt4_2),
    YYY_TEST(testlt4_3),
    
    YYY_TEST(testsqrt4_1),
    YYY_TEST(testsqrt4_2),
    YYY_TEST(testsqrt4_3),
    YYY_TEST(testsqrt4_4)
};

/* This creates the test function for this module. */
YYY_TEST_FUNCTION(YYY_ZZMPTest, zzmp_tests, "ZZMP Tests")

/* Call that function somewhere (The first argument to 
    YYY_TEST_FUNCTION is the name of the function). That runs the tests.*/

int main(int argc, char *argv[]){
    union YYY_TestResults results = { 0 };
    int ok;
    unsigned i, e;
    ok = 0;
    if(argc > 1){
        for(i = 1; i < (unsigned)argc; i++){
            for(e = 0; e < sizeof(zzmp_tests)/sizeof(*zzmp_tests); e++){
                if(strcmp(zzmp_tests[e].name, argv[i]) == 0){
                    results.s[1]++;
                    YYY_RUN_TEST(zzmp_tests[e], results.s[0], "User Specified");
                }
            }
        }
        return results.s[1] - results.s[0];
    }
    else{
        YYY_RUN_TEST_SUITE_TO(YYY_ZZMPTest, ok, "ZZMP", results);
    }
    return ok;
}

